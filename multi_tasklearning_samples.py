import re
import os
import sys
import time
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
import argparse
from pycm import *
from sklearn.metrics import classification_report
from torch.utils.data import DataLoader

from transformers import AdamW, BertForSequenceClassification, BertTokenizer
from Models import Multitask_model
from torch.utils.data.dataset import ConcatDataset
from torch.utils.data.sampler import RandomSampler
from yield_data_samples import *
import matplotlib.pyplot as plt
# Create Data
class MyDataset(torch.utils.data.Dataset):
    "Characterizes a dataset for PyTorch"

    def __init__(self, samples, labels, task_id, task_name):
        "Initialization"
        self.labels = labels
        self.samples = samples
        self.task_id = task_id
        self.task_name = task_name

    def __len__(self):
        "Denotes the total number of samples"
        return len(self.samples)

    def __getitem__(self, index):
        "Generates one sample of data"
        # Select sample
        # Load data and get label
        X = self.samples[index]
        y = self.labels[index]

        return self.task_name, X, y

class BalancedBatchSchedulerSampler(torch.utils.data.sampler.Sampler):
    """
    iterate over tasks and provide a balanced batch per task in each mini-batch
    """
    def __init__(self, dataset, batch_size):
        self.dataset = dataset
        self.batch_size = batch_size
        self.number_of_datasets = len(dataset.datasets)
        self.largest_dataset_size = max([len(cur_dataset.samples) for cur_dataset in dataset.datasets])

    def __len__(self):
        return self.batch_size * math.ceil(self.largest_dataset_size / self.batch_size) * len(self.dataset.datasets)

    def __iter__(self):
        samplers_list = []
        sampler_iterators = []
        for dataset_idx in range(self.number_of_datasets):
            cur_dataset = self.dataset.datasets[dataset_idx]
            sampler = RandomSampler(cur_dataset)
            samplers_list.append(sampler)
            cur_sampler_iterator = sampler.__iter__()
            sampler_iterators.append(cur_sampler_iterator)

        push_index_val = [0] + self.dataset.cumulative_sizes[:-1]
        step = self.batch_size * self.number_of_datasets
        samples_to_grab = self.batch_size
        # for this case we want to get all samples in dataset, this force us to resample from the smaller datasets
        epoch_samples = self.largest_dataset_size * self.number_of_datasets

        final_samples_list = []  # this is a list of indexes from the combined dataset
        for _ in range(0, epoch_samples, step):
            for i in range(self.number_of_datasets):
                cur_batch_sampler = sampler_iterators[i]
                cur_samples = []
                for _ in range(samples_to_grab):
                    try:
                        cur_sample_org = cur_batch_sampler.__next__()
                        cur_sample = cur_sample_org + push_index_val[i]
                        cur_samples.append(cur_sample)
                    except StopIteration:
                        # got to the end of iterator - restart the iterator and continue to get samples
                        # until reaching "epoch_samples"
                        sampler_iterators[i] = samplers_list[i].__iter__()
                        cur_batch_sampler = sampler_iterators[i]
                        cur_sample_org = cur_batch_sampler.__next__()
                        cur_sample = cur_sample_org + push_index_val[i]
                        cur_samples.append(cur_sample)
                final_samples_list.extend(cur_samples)

        return iter(final_samples_list)

def set_seed(args):
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if args.n_gpu > 0:
        torch.cuda.manual_seed_all(args.seed)

# Creation of the vocabulary database for one-hot vectors

def train_model(data_loader):
    model.train()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-5)
    optimizer_TS  = dict()
    for layer_name in model.task_specific_layers:
        optimizer_TS[layer_name] = torch.optim.Adam(model.task_specific_layers[layer_name].parameters(), lr=1e-5)
    start_time = time.time()
    total_loss = 0 
    print("Inside train model")
    for i, data in enumerate(data_loader):
        optimizer.zero_grad()
        for layer_name in model.task_specific_layers:
            optimizer_TS[layer_name].zero_grad()
        task_name, X,y = data[0][0], data[1], data[2]
        #print("Debug:\n task_id:{}\n X:{}\ny:{}".format(task_name, X,y))
        #print("Debug:\n task_id:{}\n X:{}\ny:{}".format(task_name, X,y))
        encoding = tokenizer.batch_encode_plus(X, return_tensors='pt', padding=True, truncation=True,max_length=200, add_special_tokens = True)
        input_ids = encoding['input_ids']
        attention_mask = encoding['attention_mask']
        
        outputs = model(task_name, input_ids.to(args.device), attention_mask=attention_mask.to(args.device))
        loss = criterion(outputs,y.cuda())
        
        loss.backward()
        optimizer.step()
        optimizer_TS[task_name].step()
        total_loss += loss.item()
        if i%1000 == 0 or (i-1)%1000 == 0 or (i-2)%1000 == 0 or (i-3)%1000 == 0:
            print('train batch {}\t\ttask {}\t\t loss {}'.format(i, task_name, loss))

    if i==0:
        i=1
    return total_loss/i


def evaluate(data_loader):
    model.eval()
    total_loss = 0.0
    y_targets = []
    y_predictions = []
    for i, data in enumerate(data_loader):
        task_name, X,y = data[0][0], data[1], data[2]
        y_targets.extend(y.tolist())

        encoding = tokenizer.batch_encode_plus(X, return_tensors='pt', padding=True, truncation=True,max_length=200, add_special_tokens = True)
        input_ids = encoding['input_ids']
        attention_mask = encoding['attention_mask']
       
        outputs = model(task_name, input_ids.to(args.device), attention_mask=attention_mask.to(args.device))
        loss = criterion(outputs,y.cuda())
        
        total_loss += loss.item()
        predictions = outputs.argmax(dim=-1)
        y_predictions.extend(predictions.tolist())

    cm = ConfusionMatrix(actual_vector=y_targets, predict_vector=y_predictions)
    F1 = cm.F1_Macro

    #return (total_loss / (i*batch_size)), F1
    if i==0:
        i=1
    return (total_loss / i), F1


def predict(data_loader, report_file, result_file): 
    y_targets = []
    y_predictions = []
    for i, data in enumerate(data_loader):
        task_name, X,y = data[0][0], data[1], data[2]
        y_targets.extend(y.tolist())
        encoding = tokenizer.batch_encode_plus(X, return_tensors='pt', padding=True, truncation=True,max_length=200, add_special_tokens = True)
        input_ids = encoding['input_ids']
        attention_mask = encoding['attention_mask']
        outputs = model(task_name, input_ids.to(args.device), attention_mask=attention_mask.to(args.device))
        predictions = outputs.argmax(dim=-1)
        #print(predictions)
        y_predictions.extend(predictions.tolist())

    #print("targets:", y_targets)
    #print("predictions:", y_predictions)
    cm = ConfusionMatrix(actual_vector=y_targets, predict_vector=y_predictions)
    out_df=pd.DataFrame(list(zip(y_targets,y_predictions)), columns=['target','predictions'])
    out_df.to_csv(result_file, header=True, index=False, sep='\t')
    with open(report_file, "w") as f: 
        f.write(str(cm))
        f.write("Classification Report:\n"+str(classification_report(y_targets, y_predictions)))

        f.write("\n\n")
        f.write("accuracy:"+str(cm.Overall_ACC))
        f.write("\nmacro F1:"+str(cm.F1_Macro))
        f.write("\nweighted F1:"+str(cm.weighted_average("F1")))
        f.write("\n")
    return None


parser = argparse.ArgumentParser()
parser.add_argument(
    "--batch_size", type=int, default=32, metavar="N", help="batch size")
parser.add_argument("--seed", type=int, default=0, help="random seed for initialization")
parser.add_argument("--cuda", action="store_true", help="Avoid using CUDA when available")
parser.add_argument("--plot", action="store_true", help="To plot the graph")
parser.add_argument("--epochs", type=int, default=5, help="upper epoch limit")
parser.add_argument("--patience", type=int, default=3, help="upper epoch limit")
parser.add_argument("--train_mtdnn", action="store_true", help="first round of training")
parser.add_argument("--train_percent", type=float, help="percentage of train data to be used", default=100)
parser.add_argument("--train_samples", type=int, help="percentage of train data to be used", default=100)
parser.add_argument('--train_datasets', default='davidson,founta,waseem,hateval')

parser.add_argument("--evaluate_dataset", type=str, help="Evaluate MTDNN on a dataset", choices= ['davidson','founta','waseem','hateval'], default=None)
parser.add_argument('--pretrained_datasets', help='list of datasets used for pretraining',default='davidson,founta,waseem,hateval')
parser.add_argument("--mtdnn_finetuned", action="store_true", help="Evaluate on send round of fine-tuned model")

parser.add_argument("--finetune_mtdnn", type=str, help="second round of finetuning type has to be davidson,founta,waseem,hateval, task on which the pretrained model to be finetuned", required=False, choices= ['davidson','founta','waseem','hateval','davidson_founta_combined','waseem_hateval_combined','waseem_hateval_wikipedia_combined', 'wikipedia', 'sst2', 'amazon'])
parser.add_argument("--adapt_model", type=str, help="adapting the pretrained MTDNN on the new tassk type has to be davidson,founta,waseem,hateval, task on which the pretrained model to be finetuned", required=False, choices= ['davidson','founta','waseem','hateval','davidson_founta_combined','waseem_hateval_combined','waseem_hateval_wikipedia_combined', 'wikipedia', 'sst2', 'amazon'])

parser.add_argument('--test_datasets', default='davidson,founta,waseem,hateval')
parser.add_argument('--pretrained_model_name', default='bert-base-uncased')
parser.add_argument('--task_layer_type', type=str, default='linear', choices=['linear', 'bilstm', 'linear_avg', 'bert_multilayer_linear'])

args = parser.parse_args()

args.device = torch.device("cuda" if torch.cuda.is_available() and args.cuda else "cpu")
args.n_gpu = 0 if not args.cuda else torch.cuda.device_count()



train_dataset = dict()
dev_dataset = dict()
test_dataset = dict()
task_name_id = dict() ## Map the task name to a id
train_dataloader = dict()
dev_dataloader = dict()
test_dataloader = dict()
train_dataloader_individual = dict()

task_list = dict() ## {name_task:num_class,....} To create the task specific layer

set_seed(args)
batch_size=args.batch_size

BASE_DIRECTORY = '/home/adsa/dataset/MTL/'
model_name = 'model_'+args.task_layer_type+'.pt'
model_ts_name = 'model_ts_'+args.task_layer_type

if args.train_percent>=1:
    args.train_percent = int(args.train_percent)

if args.train_mtdnn:
    train_dataset_list = args.train_datasets.split(',')
    test_dataset_list = args.test_datasets.split(',')

    train_dataset_list.sort()
    test_dataset_list.sort()

    print("train_dataset_list:",train_dataset_list)
    print("test_dataset_list:",test_dataset_list)

    train_dataset_all = []

    for task_id, dataset_name in enumerate(train_dataset_list):
        task_name_id[dataset_name] = task_id
        train_data, dev_data,test_data,n_class = yield_data(dataset=dataset_name, train_samples=args.train_samples, test_program=False)
        train_dataset[dataset_name] = MyDataset(train_data["sentence"].values, train_data["class"].values, task_id, dataset_name)
        dev_dataset[dataset_name] = MyDataset(dev_data["sentence"].values, dev_data["class"].values, task_id, dataset_name)
        dev_dataloader[dataset_name] = DataLoader(dev_dataset[dataset_name], batch_size=batch_size, shuffle=False)

        train_dataset_all.append(train_dataset[dataset_name])
        train_dataloader_individual[dataset_name] = DataLoader(train_dataset[dataset_name], batch_size=batch_size, shuffle=False)
        task_list[dataset_name]=n_class

    train_dataset_all = ConcatDataset(train_dataset_all)
    print('task_list:', task_list)
    for dataset_name in test_dataset_list:
        if 'combined' in dataset_name:
            sub_datasets = dataset_name.split('_')[:-1]
            for sub_dataset in sub_datasets:
                train_data, dev_data,test_data,n_class = yield_data(dataset=sub_dataset, train_samples=args.train_samples, test_program=False)
                test_dataset[sub_dataset] = MyDataset(test_data["sentence"].values, test_data["class"].values, task_name_id[dataset_name], dataset_name)
                test_dataloader[sub_dataset] = DataLoader(test_dataset[sub_dataset], batch_size=batch_size, shuffle=False)
        else:
            train_data, dev_data,test_data,n_class = yield_data(dataset=dataset_name, train_samples=args.train_samples, test_program=False)
            test_dataset[dataset_name] = MyDataset(test_data["sentence"].values, test_data["class"].values, task_name_id[dataset_name], dataset_name)
            test_dataloader[dataset_name] = DataLoader(test_dataset[dataset_name], batch_size=batch_size, shuffle=False)

    train_dataloader = DataLoader(dataset=train_dataset_all,
                                sampler=BalancedBatchSchedulerSampler(dataset=train_dataset_all,
                                                                    batch_size=batch_size),
                                batch_size=batch_size,
                                shuffle=False)

    model_dir = BASE_DIRECTORY+args.pretrained_model_name+'/'+('_'.join(train_dataset_list))+'/train_samples_'+str(args.train_samples)+'/seed_'+str(args.seed)+'/'
    model_file = model_dir + model_name
    model_ts_file = dict()

    for layer_name in train_dataset_list:
        model_ts_file[layer_name]= model_dir + model_ts_name + '_' + layer_name +'.pt'

    model = Multitask_model(args.pretrained_model_name, task_list, args.task_layer_type)
    model.to(args.device)
    #print("Debus: TS layers:", model.task_specific_layers)
    for layer_name in model.task_specific_layers:
        model.task_specific_layers[layer_name].to(args.device)

    print("model:", model)
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    
    tokenizer = BertTokenizer.from_pretrained(args.pretrained_model_name, do_lower_case=True)
    criterion = nn.CrossEntropyLoss()

    # At any point you can hit Ctrl + C to break out of training early.
    try:
        best_val_loss = None
        best_val_f1 = None
        patience = 0
        train_loss_list = dict()
        val_loss_list = dict()
        train_loss_list['combined'], val_loss_list['combined'] =[],[]

        for dataset_name in train_dataset_list:
            train_loss_list[dataset_name], val_loss_list[dataset_name] = [], []
        for epoch in range(1, args.epochs + 1):
            epoch_start_time = time.time()
            train_loss_combined = train_model(train_dataloader)
            total_val_f1 = 0
            total_val_loss = 0
            
            with torch.no_grad():
                print("-" * 89)
                for dataset_name in train_dataset_list:
                    val_loss,val_f1 = evaluate(dev_dataloader[dataset_name])
                    train_loss, _ = evaluate(train_dataloader_individual[dataset_name])
                    print('dataset_name:', dataset_name)
                    print('train_loss:',train_loss)
                    print('val_loss:',val_loss)
                    val_loss_list[dataset_name].append(val_loss)
                    train_loss_list[dataset_name].append(train_loss)
                    total_val_f1 += val_f1
                    total_val_loss += val_loss
                    print("| end of epoch {:3d} | dataset {} |time: {:5.2f}s | valid loss {:5.2f} | "
                        "val F1 {:8.2f}".format(epoch, dataset_name, (time.time() - epoch_start_time), val_loss, val_f1))
                print("-" * 89)
            # Save the model if the validation loss is the best we've seen so far.

            train_loss_list['combined'].append(train_loss_combined)
            val_loss_list['combined'].append(total_val_loss/len(train_dataset_list))
            
            patience+=1
            if not best_val_f1 or total_val_f1/len(train_dataset_list) > best_val_f1:
                patience = 0
                best_val_f1 = total_val_f1/len(train_dataset_list)
                torch.save(model, model_file)
                for layer_name in train_dataset_list:
                    torch.save(model.task_specific_layers[layer_name], model_ts_file[layer_name])
                print("Best val F1:", best_val_f1)
                print("---------------model Saved-------------")
                #print("shared:", torch.sum(torch.tensor(list(model.shared_layer.parameters())[-3])))
                #print("TS:", torch.sum(torch.tensor(list(model.task_specific_layers["davidson"].linear.weight)[-1])))
            if patience>args.patience:
                print("Early stopping due to patience")
                break

    except KeyboardInterrupt:
        print("-" * 89)
        print("Exiting from training early")

    ### Test on Data
    model = torch.load(model_file)
    for layer_name in train_dataset_list:
        model.task_specific_layers[layer_name] = torch.load(model_ts_file[layer_name])
    print("Debug: model loaded")
    #print("shared:", torch.sum(torch.tensor(list(model.shared_layer.parameters())[-3])))
    #print("TS:", torch.sum(torch.tensor(list(model.task_specific_layers["davidson"].linear.weight)[-1])))
    with torch.no_grad():
        for dataset_name in test_dataset_list:
            if 'combined' in dataset_name:
                sub_datasets = dataset_name.split('_')[:-1]
                for sub_dataset in sub_datasets:
                    test_report_file = model_dir + sub_dataset+'_'+args.task_layer_type+'_report.txt'
                    result_file = model_dir + sub_dataset+'_'+args.task_layer_type+'_result.tsv'
                    predict(test_dataloader[sub_dataset], test_report_file, result_file)
            else:
                test_report_file = model_dir + dataset_name+'_'+args.task_layer_type+'_report.txt'
                result_file = model_dir + dataset_name+'_'+args.task_layer_type+'_result.tsv'
                predict(test_dataloader[dataset_name], test_report_file, result_file)

    if args.plot:
        x_axis = range(len(train_loss_list['combined']))
        if len(train_dataset_list)>1:
            plt.plot(x_axis, train_loss_list['combined'], '-', label='train_combined')
            plt.plot(x_axis, val_loss_list['combined'], '-', label='val_combined')
        
        for dataset_name in train_dataset_list:
            y_axis_train = train_loss_list[dataset_name]
            y_axis_val = val_loss_list[dataset_name]
            plt.plot(x_axis, y_axis_train, '--', label='train_'+dataset_name)
            plt.plot(x_axis, y_axis_val, '--', label='val_'+dataset_name)
        plt.xlabel('Epoch') 
        plt.ylabel('Loss')
        plt.legend()
        plt.savefig(model_dir+'loss_plot.svg')


if args.finetune_mtdnn:
    pretrained_dataset_list = args.pretrained_datasets.split(',')
    pretrained_dataset_list.sort()

    pretrained_model_dir = BASE_DIRECTORY+args.pretrained_model_name+'/'+('_'.join(pretrained_dataset_list))+'/train_samples_'+str(args.train_samples)+'/seed_'+str(args.seed)+'/'
    pretrained_model = pretrained_model_dir + model_name

    model_dir = BASE_DIRECTORY+args.pretrained_model_name+'/'+('_'.join(pretrained_dataset_list))+'/train_samples_'+str(args.train_samples)+'/seed_'+str(args.seed)+'/mtdnn_fine_tuned_'+args.finetune_mtdnn+'/'
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    model_file = model_dir + model_name
    pretrained_model_ts_file = dict()
    model_ts_file = dict()

    for layer_name in pretrained_dataset_list:
        pretrained_model_ts_file[layer_name] = pretrained_model_dir + model_ts_name + '_' + layer_name +'.pt'
        model_ts_file[layer_name]= model_dir + model_ts_name + '_' + layer_name +'.pt'
    
    for task_id, dataset_name in enumerate(pretrained_dataset_list):
        task_name_id[dataset_name] = task_id

        train_data, dev_data,test_data,n_class = yield_data(dataset=dataset_name, train_samples=args.train_samples, test_program=False)
        if args.finetune_mtdnn == dataset_name:

            train_dataset[dataset_name] = MyDataset(train_data["sentence"].values, train_data["class"].values, task_id, dataset_name)
            dev_dataset[dataset_name] = MyDataset(dev_data["sentence"].values, dev_data["class"].values, task_id, dataset_name)

            if 'combined' in dataset_name:
                sub_datasets = dataset_name.split('_')[:-1]
                for sub_dataset in sub_datasets:
                    train_data, dev_data,test_data,n_class = yield_data(dataset=sub_dataset, train_samples=args.train_samples, test_program=False)
                    test_dataset[sub_dataset] = MyDataset(test_data["sentence"].values, test_data["class"].values, task_name_id[dataset_name], dataset_name)
                    test_dataloader[sub_dataset] = DataLoader(test_dataset[sub_dataset], batch_size=batch_size, shuffle=False)
            else:
                test_dataset[dataset_name] = MyDataset(test_data["sentence"].values, test_data["class"].values, task_name_id[dataset_name], dataset_name)
                test_dataloader[dataset_name] = DataLoader(test_dataset[dataset_name], batch_size=batch_size, shuffle=False)

            train_dataloader[dataset_name] = DataLoader(train_dataset[dataset_name], batch_size=batch_size, shuffle=False)
            dev_dataloader[dataset_name] = DataLoader(dev_dataset[dataset_name], batch_size=batch_size, shuffle=False)

        task_list[dataset_name]=n_class
    print('test_dataset:', test_dataset)
    model = Multitask_model(args.pretrained_model_name, task_list,args.task_layer_type)
    model = torch.load(pretrained_model)
    for layer_name in pretrained_dataset_list:
        model.task_specific_layers[layer_name] = torch.load(pretrained_model_ts_file[layer_name])
    model.to(args.device)
    print("pretrained_model:", model)
    #print("Debus: TS layers:", model.task_specific_layers)
    
    #print("shared:", torch.sum(torch.tensor(list(model.shared_layer.parameters())[-3])))
    #print("TS:", torch.sum(torch.tensor(list(model.task_specific_layers["davidson"].linear.weight)[-1])))

    for layer_name in model.task_specific_layers:
        model.task_specific_layers[layer_name].to(args.device)

    tokenizer = BertTokenizer.from_pretrained(args.pretrained_model_name, do_lower_case=True)
    criterion = nn.CrossEntropyLoss()

    # At any point you can hit Ctrl + C to break out of training early.
    try:
        best_val_loss = None
        best_val_f1 = None
        patience = 0

        for epoch in range(1, args.epochs + 1):
            epoch_start_time = time.time()
            train_model(train_dataloader[args.finetune_mtdnn])

            with torch.no_grad():
                print("-" * 89)
                val_loss,val_f1 = evaluate(dev_dataloader[args.finetune_mtdnn])
                print("| end of epoch {:3d} | dataset {} |time: {:5.2f}s | valid loss {:5.2f} | "
                    "val F1 {:8.2f}".format(epoch, args.finetune_mtdnn, (time.time() - epoch_start_time), val_loss, val_f1))
                print("-" * 89)
            # Save the model if the validation loss is the best we've seen so far.
            patience+=1
            if not best_val_f1 or val_f1 > best_val_f1:
                patience = 0
                best_val_f1 = val_f1
                torch.save(model, model_file)
                torch.save(model.task_specific_layers[args.finetune_mtdnn], model_ts_file[args.finetune_mtdnn])
                print("---------------model Saved-------------")
                #print("shared:", torch.sum(torch.tensor(list(model.shared_layer.parameters())[-3])))
                #print("TS:", torch.sum(torch.tensor(list(model.task_specific_layers["davidson"].linear.weight)[-1])))

            if patience>args.patience:
                print("Early stopping due to patience")
                break

    except KeyboardInterrupt:
        print("-" * 89)
        print("Exiting from training early")

    ### Test on Data
    model = torch.load(model_file)
    model.task_specific_layers[args.finetune_mtdnn] = torch.load(model_ts_file[args.finetune_mtdnn])    
    
    print("Debug: Model loaded")

    #print("shared:", torch.sum(torch.tensor(list(model.shared_layer.parameters())[-3])))
    #print("TS:", torch.sum(torch.tensor(list(model.task_specific_layers["davidson"].linear.weight)[-1])))
    with torch.no_grad():
        if 'combined' in args.finetune_mtdnn:
            sub_datasets = args.finetune_mtdnn.split('_')[:-1]
            for sub_dataset in sub_datasets:
                test_report_file = model_dir + sub_dataset+'_'+args.task_layer_type+'_report.txt'
                result_file = model_dir + sub_dataset+'_'+args.task_layer_type+'_result.tsv'
                predict(test_dataloader[sub_dataset], test_report_file, result_file)
        else:
            test_report_file = model_dir + args.finetune_mtdnn +'_'+args.task_layer_type+'_report.txt'
            result_file = model_dir + args.finetune_mtdnn +'_'+args.task_layer_type+'_result.tsv'
            predict(test_dataloader[args.finetune_mtdnn], test_report_file, result_file)

if args.adapt_model:
    pretrained_dataset_list = args.pretrained_datasets.split(',')
    pretrained_dataset_list.sort()

    #pretrained_model_dir = BASE_DIRECTORY+args.pretrained_model_name+'/'+('_'.join(pretrained_dataset_list))+'/train_samples_'+str(args.train_samples)+'/seed_'+str(args.seed)+'/'
    pretrained_model_dir = BASE_DIRECTORY+args.pretrained_model_name+'/'+('_'.join(pretrained_dataset_list))+'/train_percent_100/seed_'+str(args.seed)+'/'
    pretrained_model = pretrained_model_dir + model_name

    model_dir = BASE_DIRECTORY+args.pretrained_model_name+'/'+('_'.join(pretrained_dataset_list))+'/train_percent_100/seed_'+str(args.seed)+'/mtdnn_adapt_'+args.adapt_model+'/'+'adapt_samples_'+str(args.train_samples)+'/'
    
    
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    model_file = model_dir + model_name
    pretrained_model_ts_file = dict()
    model_ts_file = dict()

    for layer_name in pretrained_dataset_list:
        pretrained_model_ts_file[layer_name] = pretrained_model_dir + model_ts_name + '_' + layer_name +'.pt'
    
    model_ts_file[args.adapt_model]= model_dir + model_ts_name + '_' + args.adapt_model +'.pt'

    pretrained_dataset_list.append(args.adapt_model)
    pretrained_dataset_list.sort()
 
    for task_id, dataset_name in enumerate(pretrained_dataset_list):
        task_name_id[dataset_name] = task_id

        train_data, dev_data,test_data,n_class = yield_data(dataset=dataset_name, train_samples=args.train_samples, test_program=False)

        if args.adapt_model == dataset_name:
            train_dataset[dataset_name] = MyDataset(train_data["sentence"].values, train_data["class"].values, task_id, dataset_name)
            dev_dataset[dataset_name] = MyDataset(dev_data["sentence"].values, dev_data["class"].values, task_id, dataset_name)

            if 'combined' in dataset_name:
                sub_datasets = dataset_name.split('_')[:-1]
                for sub_dataset in sub_datasets:
                    train_data, dev_data,test_data,n_class = yield_data(dataset=sub_dataset, train_samples=args.train_samples, test_program=False)
                    test_dataset[sub_dataset] = MyDataset(test_data["sentence"].values, test_data["class"].values, task_name_id[dataset_name], dataset_name)
                    test_dataloader[sub_dataset] = DataLoader(test_dataset[sub_dataset], batch_size=batch_size, shuffle=False)
            else:
                test_dataset[dataset_name] = MyDataset(test_data["sentence"].values, test_data["class"].values, task_name_id[dataset_name], dataset_name)
                test_dataloader[dataset_name] = DataLoader(test_dataset[dataset_name], batch_size=batch_size, shuffle=False)

            train_dataloader[dataset_name] = DataLoader(train_dataset[dataset_name], batch_size=batch_size, shuffle=False)
            dev_dataloader[dataset_name] = DataLoader(dev_dataset[dataset_name], batch_size=batch_size, shuffle=False)

        task_list[dataset_name]=n_class

    print("Debug: task_list:", task_list)
    model = Multitask_model(args.pretrained_model_name, task_list,args.task_layer_type)

    model = torch.load(pretrained_model)
    for layer_name in pretrained_dataset_list:
        if layer_name not in args.adapt_model:
            model.task_specific_layers[layer_name] = torch.load(pretrained_model_ts_file[layer_name])
    model.to(args.device)

    model.add_layer(args.adapt_model, task_list[args.adapt_model])

    print("Debus: TS layers:", model.task_specific_layers)

    #print("shared:", torch.sum(torch.tensor(list(model.shared_layer.parameters())[-3])))
    #print("TS:", torch.sum(torch.tensor(list(model.task_specific_layers["davidson"].linear.weight)[-1])))

    for layer_name in model.task_specific_layers:
        model.task_specific_layers[layer_name].to(args.device)

    tokenizer = BertTokenizer.from_pretrained(args.pretrained_model_name, do_lower_case=True)
    criterion = nn.CrossEntropyLoss()

    # At any point you can hit Ctrl + C to break out of training early.
    try:
        best_val_loss = None
        best_val_f1 = None
        patience = 0

        for epoch in range(1, args.epochs + 1):
            epoch_start_time = time.time()
            train_model(train_dataloader[args.adapt_model])

            with torch.no_grad():
                print("-" * 89)
                val_loss,val_f1 = evaluate(dev_dataloader[args.adapt_model])
                print("| end of epoch {:3d} | dataset {} |time: {:5.2f}s | valid loss {:5.2f} | "
                    "val F1 {:8.2f}".format(epoch, args.adapt_model, (time.time() - epoch_start_time), val_loss, val_f1))
                print("-" * 89)
            # Save the model if the validation loss is the best we've seen so far.
            patience+=1
            if not best_val_f1 or val_f1 > best_val_f1:
                patience = 0
                best_val_f1 = val_f1
                torch.save(model, model_file)
                torch.save(model.task_specific_layers[args.adapt_model], model_ts_file[args.adapt_model])
                print("---------------model Saved-------------")

            if patience>args.patience:
                print("Early stopping due to patience")
                break

    except KeyboardInterrupt:
        print("-" * 89)
        print("Exiting from training early")

    ### Test on Data
    model = torch.load(model_file)
    model.task_specific_layers[args.adapt_model] = torch.load(model_ts_file[args.adapt_model])

    print("Debug: Model loaded")
    #print("shared:", torch.sum(torch.tensor(list(model.shared_layer.parameters())[-3])))
    #print("TS:", torch.sum(torch.tensor(list(model.task_specific_layers["davidson"].linear.weight)[-1])))


    with torch.no_grad():
        if 'combined' in args.adapt_model:
            sub_datasets = args.adapt_model.split('_')[:-1]
            for sub_dataset in sub_datasets:
                test_report_file = model_dir + sub_dataset+'_'+args.task_layer_type+'_report.txt'
                result_file = model_dir + sub_dataset+'_'+args.task_layer_type+'_result.tsv'
                predict(test_dataloader[sub_dataset], test_report_file)
        else:
            test_report_file = model_dir + args.adapt_model +'_'+args.task_layer_type+'_report.txt'
            result_file = model_dir + args.adapt_model+'_'+args.task_layer_type+'_result.tsv'
            predict(test_dataloader[args.adapt_model], test_report_file, result_file)
        print("TR file:", test_report_file)


if args.evaluate_dataset:

    pretrained_dataset_list = args.pretrained_datasets.split(',')
    pretrained_dataset_list.sort()
    
    for task_id, dataset_name in enumerate(pretrained_dataset_list):
        task_name_id[dataset_name] = task_id

        train_data, dev_data,test_data,n_class = yield_data(dataset=dataset_name, train_samples=args.train_samples, test_program=False)
        
        if args.evaluate_dataset == dataset_name:
            test_dataset[dataset_name] = MyDataset(test_data["sentence"].values, test_data["class"].values, task_name_id[dataset_name], dataset_name)
            test_dataloader[dataset_name] = DataLoader(test_dataset[dataset_name], batch_size=batch_size, shuffle=False)

        task_list[dataset_name]=n_class
    
    model_dir = BASE_DIRECTORY+args.pretrained_model_name+'/'+('_'.join(pretrained_dataset_list))+'/train_samples_'+str(args.train_samples)+'/seed_'+str(args.seed)+'/'

    if args.mtdnn_finetuned:
        model_dir += 'mtdnn_fine_tuned_'+args.evaluate_dataset+ '/'


    pretrained_model = model_dir + model_name
    print("pretrained_model:",pretrained_model)
    model = Multitask_model(args.pretrained_model_name, task_list, args.task_layer_type)
    model = torch.load(pretrained_model)
    model.to(args.device)
    #print("Debus: TS layers:", model.task_specific_layers['hateval'].linear.weight.data[0])

    for layer_name in model.task_specific_layers:
        model.task_specific_layers[layer_name].to(args.device)

    tokenizer = BertTokenizer.from_pretrained(args.pretrained_model_name, do_lower_case=True)

    test_report_file = model_dir + args.evaluate_dataset+'_'+args.task_layer_type+'_report.txt'
    result_file = model_dir + args.evaluate_dataset+'_'+args.task_layer_type+'_result.tsv'
    predict(test_dataloader[args.evaluate_dataset], test_report_file, result_file)

    print("model evaluation logged in file:", test_report_file)
