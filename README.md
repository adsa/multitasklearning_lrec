# Multi-task (Multi-corpus) Learning

### yield_data.py 
File that provides training, development, and test sets for various corpus

### yield_data_samples.py 
File that provides training, development, and test sets for various corpus with specified number of training sampels (used for low-resource scenarios only)

### string_Preprocess.py
File that preprocesses the data for twitter and wikipedia datasets



## multi_tasklearning.py
File that runs multi-task (multi-corpus) model

#### Model Parameters

*Abbreviations:*
STL: Single-task learning
MTL: Multi-task learning

We use `bert-base-uncased` as shared layers

**--seed**, default=0 : random seed for initialization

**--batch_size**, default=32: mini-batch size

**--epochs**, default=5: Maximum number of training  epochs

**--patience**, default=3: Patience for early stopping

----
**--cuda**: To use GPU (no input parameter)

**--plot**: To plot the training and validation loss graph (no input parameter)

----
**--train_mtdnn**: STL or MTL training of model (first round of training)" (no input parameter)

**--train_percent**, default=100: percentage of train data to be used (for low-resource scenarios. 100 indicates using 100% of training data, 50% uses half of training data)

**--train_datasets**, default='davidson,founta,waseem,hateval,wikipedia': datasets to be used for STL or MTL

----
**--finetune_mtdnn**: Second round of finetuning. The dataset on which the model has to be fine-tuned should be provided (MTL_finetuned setup). --pretrained_datasets parameters has to be provided to idicate the MCL model.

```
To fine-tune on davidson dataset for model trained with davidson,founta,waseem,hateval,wikipedia datasets, use:

--finetune_mtdnn davidson --pretrained_datasets davidson,founta,waseem,hateval,wikipedia
```

----
**--adapt_model**: adapting the pretrained MTDNN on the new task(dataset) (Supervised domain adaptation) --pretrained_datasets parameters has to be provided to idicate the MCL model

```
To adapt on davidson dataset for model trained with founta,waseem,hateval,wikipedia datasets, use:

--adapt_model davidson --pretrained_datasets founta,waseem,hateval,wikipedia
```

----
**--test_datasets**: list of comma separated datasets on which the model has to be tested while training STL or MTL models

----

**--pretrained_model_name**, default='bert-base-uncased': String indicating the name of the model from Huggingface library

**--task_layer_type**, default='linear', choices=['linear', 'bilstm', 'linear_avg', 'bert_multilayer_linear']: Type of task-specific layer for MTL

*'linear'*: CLS token from last layer of BERT is used as input to linear layer 
*'linear_avg'*: Average from all the tokens from last layer of BERT is used as input to linear layer 
*'bert_multilayer_linear'*: Concatenated output of CLS token from 2,6,12th layer of BERT is used to linear layer
*'bilstm'*: output of all the tokens from last layer of BERT is used as input to Bi-LSTM layer for sequential processing

----

**--evaluate_dataset**: Evaluate MTL on a dataset, without performing training or fine-tuning of the model

**--pretrained_datasets**: List of datasets on which MTL or STL model was trained or fine-tuned

**--mtdnn_finetuned**: Evaluate on sencond round of fine-tuned model

```
To evaluate performance on *davidson* dataset on MTL model trained using davidson,founta,waseem,hateval,wikipedia datasets, use:

--evaluate_dataset davidson --pretrained_datasets davidson,founta,waseem,hateval,wikipedia
```
```
To evaluate performance on *davidson* dataset on MTL model trained using davidson,founta,waseem,hateval,wikipedia datasets, futher fine-tuned on davidson datsaset (MTL_finetuned setup):

--evaluate_dataset davidson --pretrained_datasets davidson,founta,waseem,hateval,wikipedia --mtdnn_finetuned davidson
```

### Improtant NOTE:

Before runnning the programs, resolve the below paths:

In the programs:

**yield_data.py:**
Provide proper path to `infile_` variables to point the appropriate input data file

**multi_tasklearning.py:**
Provide proper path to `BASE_DIRECTORY` variables to point the storage directory


**Execution examples:**

```
STL on davidson
python multi_tasklearning.py --train_datasets davidson --test_dataset davidson --cuda --train_mtdnn --task_layer_type linear --epochs 10 --train_percent 100 --seed 0

MTL on davidson,founta,wikipedia (3 corpus specific layers)
python multi_tasklearning.py --train_datasets  davidson,founta,wikipedia --test_dataset davidson,founta,wikipedia --cuda --train_mtdnn --task_layer_type linear --epochs 10 --train_percent 100 --seed 0

MTL_finetuned on founta with the above MTL model:
python multi_tasklearning.py --pretrained_datasets davidson,founta,wikipedia  --finetune_mtdnn founta --cuda --task_layer_type linear --epochs 10 --seed 0 --train_percent 100

Domain Adaptation on wikipedia dataset with MCL model trained on davidson,founta,hateval datasets
python multi_tasklearning.py --pretrained_datasets davidson,founta,hateval --adapt_model wikipedia --cuda --task_layer_type linear --epochs 10 --seed 0 --train_percent 100

MTL on davidson,founta,wikipedia (3 corpus specific layers) in low resource with 50% of training samples
python multi_tasklearning.py --train_datasets  davidson,founta,wikipedia --test_dataset davidson,founta,wikipedia --cuda --train_mtdnn --task_layer_type linear --epochs 10 --train_percent 50 --seed 0

MTL on davidson,founta,wikipedia (3 corpus specific layers) in low resource with 200  training samples from each dataset
python multi_tasklearning.py --train_datasets  davidson,founta,wikipedia --test_dataset davidson,founta,wikipedia --cuda --train_mtdnn --task_layer_type linear --epochs 10 --train_samples 200 --seed 0
```

**datasets:**

+ davidson
+ founta
+ hateval
+ waseem
+ wikipedia
+ davidson_founta_combined: training sets of davison and founta combined to have a single corpus specific layer
+ waseem_hateval_combined: training sets of waseem and hateval combined to have a single corpus specific layer
+ waseem_hateval_wikipedia_combined: training sets of waseem, hateval, and wikipedia combined to have a single corpus specific layer



### Outputs:

The outputs will be stored in:

**STL or MTL:**

Classification report:

`BASE_DIRECTORY/bert-base-uncased/<dataset1_dataset2..>/train_percent_<train_percent>/seed_<seed>/<dataset>_<task_specific_layer_type>_report.txt`



True label of test set and model predictions:

`BASE_DIRECTORY/bert-base-uncased/<dataset1_dataset2..>/train_percent_<train_percent>/seed_<seed>/<dataset>_<task_specific_layer_type>_result.tsv`



**MTL_finetuned:**

Classification report:

`BASE_DIRECTORY/bert-base-uncased/<dataset1_dataset2..>/train_percent_<train_percent>/seed_<seed>/mtdnn_fine_tuned_<fine-tune dataset>/<dataset>_<task_specific_layer_type>_report.txt` 


True label of test set and model predictions:

`BASE_DIRECTORY/bert-base-uncased/<dataset1_dataset2..>/train_percent_<train_percent>/seed_<seed>/mtdnn_fine_tuned_<fine-tune dataset>/<dataset>_<task_specific_layer_type>_result.tsv` 


**Domain Adaptation**:

Classification report:

`BASE_DIRECTORY/bert-base-uncased/<dataset1_dataset2..>/train_percent_100/seed_<seed>/mtdnn_adapt_<adaptation_dataset>/adapt_percent_<adaptation_percent>/<dataset>_<task_specific_layer_type>_report.txt`


True label of test set and model predictions:

`BASE_DIRECTORY/bert-base-uncased/<dataset1_dataset2..>/train_percent_100/seed_<seed>/mtdnn_adapt_<adaptation_dataset>/adapt_percent_<adaptation_percent>/<dataset>_<task_specific_layer_type>_report.txt`
